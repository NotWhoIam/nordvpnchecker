﻿using System;
using System.Collections.Generic;

namespace MainApp.Core {
    public class AccountInfo {
        public string Username { get; set; }
        public string Password { get; set; }
        public List<Service> Services { get; set; }

        public override string ToString() {
            return $"{Username}:{Password}{Environment.NewLine}\t {string.Join(Environment.NewLine + "\t ", Services)}";
        }
    }
}