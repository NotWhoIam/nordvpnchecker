﻿using System.Collections.Generic;

namespace MainApp.Core.Contracts {
    public interface ICmdLineParser {
        CmdParseResult Parse(IList<string> args);
    }
}