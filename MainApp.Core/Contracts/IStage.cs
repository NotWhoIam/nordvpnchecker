﻿namespace MainApp.Core.Contracts {
    public interface IStage {
        string Name { get; }
    }
}