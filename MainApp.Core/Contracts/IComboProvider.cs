﻿using System.Collections.Generic;

namespace MainApp.Core.Contracts {
    public interface IComboProvider {
        string FilePath { get; set; }
        IEnumerable<(string Username, string Password)> GetCredentials();
        int GetCount();
    }
}