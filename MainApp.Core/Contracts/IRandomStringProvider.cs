﻿namespace MainApp.Core.Contracts {
    public interface IRandomStringProvider {
        string GetRandomString(int length = 16);
    }
}