﻿namespace MainApp.Core.Contracts {
    public interface ITitleService {
        void SetTotal(int count);
        void Hit();
        void MissEmpty();
        void MissBad();
    }
}