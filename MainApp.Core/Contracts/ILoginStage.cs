﻿using System.Threading.Tasks;

namespace MainApp.Core.Contracts {
    public interface ILoginStage : IStage {
        Task<bool> Login(Context ctx);
    }
}