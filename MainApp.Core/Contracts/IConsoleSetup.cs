﻿using System.Collections.Generic;

namespace MainApp.Core.Contracts {
    public interface IConsoleSetup {
        CmdParseResult Setup(IList<string> args);
    }
}