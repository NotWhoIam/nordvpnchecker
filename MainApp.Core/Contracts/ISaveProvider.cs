﻿namespace MainApp.Core.Contracts {
    public interface ISaveProvider {
        string FilePath { get; set; }
        void Save(AccountInfo info);
    }
}