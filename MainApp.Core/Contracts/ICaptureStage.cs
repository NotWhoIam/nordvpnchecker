﻿using System.Threading.Tasks;

namespace MainApp.Core.Contracts {
    public interface ICaptureStage : IStage {
        Task<AccountInfo> Capture(Context ctx);
    }
}