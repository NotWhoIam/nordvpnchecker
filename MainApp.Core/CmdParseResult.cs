﻿using System.Collections.Generic;

namespace MainApp.Core {
    public class CmdParseResult {
        public string Input { get; set; }
        public ISet<CmdSwitch> Flags { get; } = new HashSet<CmdSwitch>();
        public IDictionary<CmdSwitch, string> Options { get; } = new Dictionary<CmdSwitch, string>();

        public string GetOptionOrDefault(CmdSwitch option) {
            if (!Options.TryGetValue(option, out var value))
                value = option.DefaultArgument;

            return value;
        }
    }
}