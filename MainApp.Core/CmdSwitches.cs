﻿using System;
using System.Collections.Generic;

namespace MainApp.Core {
    public static class CmdSwitches {
        public static readonly IList<CmdSwitch> AllSwitches = new List<CmdSwitch>();
        
        public static readonly CmdSwitch Help = new CmdSwitch(new[] {
            "h", "-help"
        }, "Shows help message.");
        
        public static readonly CmdSwitch Output = new CmdSwitch(new[] {
            "o", "-output"
        }, "Sets output file path.", $"Hits-{DateTime.Now:yyyy-MM-dd_HH-mm-ss}.txt");
        
        public static readonly CmdSwitch Cookies = new CmdSwitch(new[] {
            "c", "-cookies"
        }, "Sets the cookies that are used in the requests.", "Random cookie");
        
        public static readonly CmdSwitch Csrf = new CmdSwitch(new[] {
            "r", "-csrf"
        }, "Sets the CSRF token that is used in the requests.", "Random csrf");
    }
}