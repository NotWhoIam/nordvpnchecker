﻿using System.Collections.Generic;

namespace MainApp.Core {
    public class CmdSwitch {
        public CmdSwitch(IList<string> identifiers, string description) {
            CmdSwitches.AllSwitches.Add(this);
            Identifiers = identifiers;
            Description = description;
        }

        public CmdSwitch(IList<string> identifiers, string description, string defaultArgument) 
                        : this(identifiers, description) {
            HasArgument = true;
            DefaultArgument = defaultArgument;
        }
        
        public IList<string> Identifiers { get; }
        public string Description { get; }
        public bool HasArgument { get; }
        public string DefaultArgument { get; }
    }
}