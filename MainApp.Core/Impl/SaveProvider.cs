﻿using System.IO;
using MainApp.Core.Contracts;

namespace MainApp.Core.Impl {
    public class SaveProvider : ISaveProvider {
        ~SaveProvider() {
            _writer.Close();
        }
        
        public string FilePath { get; set; }
        
        private StreamWriter _writer;
        
        public void Save(AccountInfo info) {
            if (_writer == null) {
                EnsureOutputDirectory();
                _writer = new StreamWriter(File.Open(FilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite)) {
                    AutoFlush = true
                };
            }
            
            _writer.WriteLine(info.ToString());
        }

        private void EnsureOutputDirectory() {
            Directory.CreateDirectory(Path.GetDirectoryName(Path.GetFullPath(FilePath)));
        }
    }
}