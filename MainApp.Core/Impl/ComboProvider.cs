﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MainApp.Core.Contracts;

namespace MainApp.Core.Impl {
    //This has laughable performance (it was intended because this is public software)
    public class ComboProvider : IComboProvider {
        public string FilePath { get; set; }

        private StreamReader _reader;

        public IEnumerable<(string Username, string Password)> GetCredentials() {
            if (_reader == null)
                Initialise();

            string line;
            while ((line = _reader.ReadLine()) != null) {
                var data = line.Split(':');
                if (data.Length < 2)
                    throw new FormatException("Incorrect combo format");

                yield return (data[0], data[1]);
            }
        }

        public int GetCount() {
            if (_reader == null)
                Initialise();

            var pos = _reader.BaseStream.Position;
            var str = _reader.ReadToEnd();
            _reader.BaseStream.Position = pos;
            return str.Split('\n', StringSplitOptions.RemoveEmptyEntries).Length;
        }
        
        private void Initialise() {
            if (string.IsNullOrEmpty(FilePath) || !File.Exists(FilePath))
                throw new ArgumentException($"Couldn't find the path: '{FilePath}'", nameof(FilePath));

            var stream = File.OpenRead(FilePath);
            _reader = new StreamReader(stream);
        }
    }
}