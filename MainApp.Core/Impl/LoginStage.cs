﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MainApp.Core.Contracts;

namespace MainApp.Core.Impl {
    public class LoginStage : ILoginStage {
        public LoginStage(IHttpClientFactory factory, IRandomStringProvider provider) {
            _fac = factory;
            _prov = provider;
        }
        
        public string Name => "Login stage";
        
        private readonly IHttpClientFactory _fac;
        private readonly IRandomStringProvider _prov;
        
        public async Task<bool> Login(Context ctx) {
            var result = await LoginPrivate(ctx);
            var good = result.StatusCode == HttpStatusCode.OK;

            if (good) {
                result.Headers.TryGetValues("set-cookie", out var values);
                ctx.SetToken(values);
            }

            return good;
        }

        private async Task<HttpResponseMessage> LoginPrivate(Context ctx) {
            var client = _fac.CreateClient("users");
            var message = CreateLoginRequest(ctx, client);

            return await client.SendAsync(message, HttpCompletionOption.ResponseHeadersRead);
        }

        private HttpRequestMessage CreateLoginRequest(Context ctx, HttpClient client) {
            var message = new HttpRequestMessage(HttpMethod.Post, client.BaseAddress);
            
            AddHeaders(ctx, message);
            AddContent(ctx, message);

            return message;
        }

        private void AddContent(Context ctx, HttpRequestMessage message) {
            message.Content = new StringContent("{" + string.Format(Constants.LoginJson, ctx.Username, ctx.Password,
                 _prov.GetRandomString()) + "}", Encoding.UTF8, "application/json");
        }

        private static void AddHeaders(Context ctx, HttpRequestMessage message) {
            message.Headers.TryAddWithoutValidation("cookie", ctx.Cookies);
            message.Headers.TryAddWithoutValidation("x-csrf-token", ctx.CsrfToken);
        }
    }
}
