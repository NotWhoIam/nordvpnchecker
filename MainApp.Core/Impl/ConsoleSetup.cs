﻿using System;
using System.Collections.Generic;
using System.Linq;
using MainApp.Core.Contracts;

namespace MainApp.Core.Impl {
    public class ConsoleSetup : IConsoleSetup {
        public ConsoleSetup(ICmdLineParser cmd) {
            _cmd = cmd;
        }

        //When OCD grows on you, boy does it get ugly
        private static string PlatformSpecificExtension {
            get {
                return Environment.OSVersion.Platform switch {
                    PlatformID.Win32NT => ".exe",
                    PlatformID.Unix => string.Empty,
                    _ => "???"
                };
            }
        }

        private readonly ICmdLineParser _cmd;
        
        public CmdParseResult Setup(IList<string> args) {
            var result = _cmd.Parse(args);
            if (result.Flags.Contains(CmdSwitches.Help) || result.Input == null)
                PrintHelp();

            return result;
        }

        private static void PrintHelp() {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Usage: ");
            Console.WriteLine($"   ./MainApp{PlatformSpecificExtension} [options] <input file> [options]");
            Console.WriteLine();
            Console.WriteLine("Available options:");
            foreach (var sw in CmdSwitches.AllSwitches.OrderBy(s => s.Identifiers.First())) {
                Console.Write("   -" + string.Join(" -", sw.Identifiers.OrderBy(i => i.Length)).PadRight(25));
                Console.WriteLine(sw.Description);
            }
            
            ReadThenExit();
        }

        private static void ReadThenExit() {
            Console.ReadLine();
            Environment.Exit(0);
        }
    }
}