﻿using System.Collections.Generic;
using MainApp.Core.Contracts;

namespace MainApp.Core.Impl {
    public class CmdLineParser : ICmdLineParser {
        static CmdLineParser() {
            foreach (var sw in CmdSwitches.AllSwitches) {
                var coll = sw.HasArgument ? _options : _flags;
                foreach (var id in sw.Identifiers)
                    coll[id] = sw;
            }    
        }

        private static readonly IDictionary<string, CmdSwitch> _flags = new Dictionary<string, CmdSwitch>();
        private static readonly IDictionary<string, CmdSwitch> _options = new Dictionary<string, CmdSwitch>();
        
        public CmdParseResult Parse(IList<string> args) {
            var res = new CmdParseResult();

            for (var i = 0; i < args.Count; i++) {
                var curr = args[i];
                if (curr[0] == '-') {
                    var word = curr.Substring(1);

                    if (_flags.TryGetValue(word, out var flag))
                        res.Flags.Add(flag);
                    
                    else if (_options.TryGetValue(word, out var option))
                        res.Options[option] = args[++i];
                    
                } else if (res.Input == null)
                    res.Input = curr;
            }
            
            return res;
        }
    }
}