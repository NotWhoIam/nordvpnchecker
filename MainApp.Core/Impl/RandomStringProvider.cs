﻿using System;
using System.Linq;
using MainApp.Core.Contracts;

namespace MainApp.Core.Impl {
    public class RandomStringProvider : IRandomStringProvider {
        private const string Alphabet = "abcdef0123456789";
        
        private readonly Random _r = new Random();
        
        public string GetRandomString(int length = 16) {
            return new string(Enumerable.Repeat(Alphabet, length).Select(s => s[_r.Next(s.Length)]).ToArray());
        }
    }
}