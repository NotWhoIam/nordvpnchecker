﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using MainApp.Core.Contracts;

namespace MainApp.Core.Impl {
    public class CaptureStage : ICaptureStage {
        public CaptureStage(IHttpClientFactory fac) {
            _fac = fac;
        }
        
        public string Name => "Capture stage";

        private readonly IHttpClientFactory _fac;
        
        public async Task<AccountInfo> Capture(Context ctx) {
            var info = new AccountInfo {
                Username = ctx.Username,
                Password = ctx.Password,
            };

            var result = await CapturePrivate(ctx);
            info.Services = await JsonSerializer.DeserializeAsync<List<Service>>(await result.Content.ReadAsStreamAsync());

            return info;
        }

        private async Task<HttpResponseMessage> CapturePrivate(Context ctx) {
            var client = _fac.CreateClient("services");
            
            AddCookies(client, ctx);
            var message = CreateCaptureRequest(client);

            return await client.SendAsync(message, HttpCompletionOption.ResponseHeadersRead);
        }

        private static HttpRequestMessage CreateCaptureRequest(HttpClient client) {
            return new HttpRequestMessage(HttpMethod.Get, client.BaseAddress);
        }

        private static void AddCookies(HttpClient client, Context ctx) {
            client.DefaultRequestHeaders.TryAddWithoutValidation("cookie", ctx.Cookies + ";token=" + ctx.Token);
        }
    }
}