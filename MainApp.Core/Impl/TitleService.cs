﻿using System;
using MainApp.Core.Contracts;

namespace MainApp.Core.Impl {
    public class TitleService : ITitleService {
        private const string BaseTitle = "NordVPN Checker by xsilent007 | Left: {0} | Hits: {1} | Empty: {2} | Bad: {3}";

        private int _total;
        private int _left;
        private int _hit;
        private int _empty;
        private int _bad;
        
        public void SetTotal(int count) {
            _total = count;
            _left = count;
            
            UpdateTitle();
        }

        public void Hit() {
            _hit++;
            _left--;
            
            UpdateTitle();
        }

        public void MissEmpty() {
            _empty++;
            _left--;
            
            UpdateTitle();
        }

        public void MissBad() {
            _bad++;
            _left--;
            
            UpdateTitle();
        }

        private void UpdateTitle() {
            Console.Title = string.Format(BaseTitle, _left, _hit, _empty, _bad);
        }
    }
}