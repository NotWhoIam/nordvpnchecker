﻿namespace MainApp.Core {
    public static class Constants {
        public const string UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36";
        public const string LoginJson = "\"username\":\"{0}\",\"password\":\"{1}\",\"session_id\":\"{2}\"";
    }
}