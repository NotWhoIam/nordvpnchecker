﻿using System.Text.Json.Serialization;

namespace MainApp.Core {
    public class ServiceInfo {
        [JsonPropertyName("name")] public string Name { get; set; }
    }
}