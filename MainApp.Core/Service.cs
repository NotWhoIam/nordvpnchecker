﻿using System;
using System.Text.Json.Serialization;

namespace MainApp.Core {
    public class Service {
        [JsonPropertyName("expires_at")] public string Expires { get; set; }
        [JsonPropertyName("active")] public bool Active { get; set; }
        [JsonPropertyName("service")] public ServiceInfo Info { get; set; }

        public override string ToString() {
            return $"Service: {Info.Name} | Active: {Active} | Expires: {DateTime.Parse(Expires):f}";
        }
    }
}