﻿using System.Collections.Generic;
using System.Text;

namespace MainApp.Core {
    public class Context {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Cookies { get; set; }
        public string CsrfToken { get; set; }
        public string Token { get; set; }

        public void SetToken(IEnumerable<string> values) {
            var real = values as string[];
            
            //ReSharper disable once PossibleNullReferenceException
            var val = real[0];
            
            var builder = new StringBuilder();
            for (var i = val.IndexOf('=') + 1; val[i] != ';'; i++)
                builder.Append(val[i]);

            Token = builder.ToString();
        }
    }
}