﻿using System.Drawing;
using Colorful;
using MainApp.Core.Contracts;

namespace MainApp {
    public class Logger : ILogger {
        public void Info(string message) {
            Log('*', message, Color.Aqua);
        }

        public void Warning(string message) {
            Log('!', message, Color.Orange);
        }

        public void Error(string message) {
            Log('#', message, Color.Red);
        }

        public void Success(string message) {
            Log('+', message, Color.Lime);
        }
        
        private static void Log(char prefix, string message, Color color) {
            Console.Write("[", Color.White);
            Console.Write(prefix, color);
            Console.Write("] ", Color.White);
            Console.WriteLine(message, color);
        }
    }
}