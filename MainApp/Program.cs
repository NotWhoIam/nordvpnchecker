﻿using System.Drawing;
using System.Threading.Tasks;
using Colorful;
using MainApp.Core.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace MainApp {
    public static class Program {
        public static void Main(string[] args) {
            var container = Startup.ConfigureServices();

            var pl = new Pipeline(
                container.GetService<IComboProvider>(),
                container.GetService<ISaveProvider>(),
                container.GetService<ILogger>(),
                container.GetService<ITitleService>(),
                container.GetService<IConsoleSetup>().Setup(args),
                () => container.GetService<ILoginStage>(),
                () => container.GetService<ICaptureStage>()
            );
            
            Task.WaitAll(pl.Run());
            Console.WriteLine("Finished!", Color.Magenta);
            Console.ReadLine();
        }
    }
}