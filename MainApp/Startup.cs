﻿using System;
using System.Net;
using System.Net.Http;
using MainApp.Core;
using MainApp.Core.Contracts;
using MainApp.Core.Impl;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Extensions.Http;

namespace MainApp {
    public static class Startup {
        private const string LoginUrl = "https://ucp.nordvpn.com/api/v1/users/login";
        private const string ServicesUrl = "https://ucp.nordvpn.com/api/v1/users/services";
        
        private static readonly ILogger _logger = new Logger();
        
        public static IServiceProvider ConfigureServices() {
            var services = new ServiceCollection();

            services.AddSingleton<IConsoleSetup, ConsoleSetup>();
            services.AddSingleton<ITitleService, TitleService>();
            services.AddSingleton<IRandomStringProvider, RandomStringProvider>();
            services.AddSingleton<IComboProvider, ComboProvider>();
            services.AddSingleton<ISaveProvider, SaveProvider>();
            services.AddSingleton<ICmdLineParser, CmdLineParser>();
            
            services.AddTransient<ILoginStage, LoginStage>();
            services.AddTransient<ICaptureStage, CaptureStage>();

            static ILogger LoggerGetter() => _logger;
            services.AddSingleton(_logger);
            
            AddClient(services, "users", LoginUrl, LoggerGetter);
            AddClient(services, "services", ServicesUrl, LoggerGetter);
            
            return new DefaultServiceProviderFactory().CreateServiceProvider(services);
        }

        private static void AddClient(IServiceCollection collection, string name, string baseUri, Func<ILogger> loggerGetter) {
            AddPolicies(collection.AddHttpClient(name, SetupClient(baseUri)), loggerGetter);
        }

        private static void AddPolicies(IHttpClientBuilder builder, Func<ILogger> loggerGetter) {
            builder.AddPolicyHandler(Policy
                .HandleResult<HttpResponseMessage>(
                    //We want to handle both of these cases manually
                    r =>
                        //Our login was successful
                        r.StatusCode != HttpStatusCode.OK
                        &&
                        //Our login was unsuccessful
                        r.StatusCode != HttpStatusCode.Unauthorized)
                .OrTransientHttpError()
                .WaitAndRetryAsync(new[] {
                    TimeSpan.FromMinutes(1),
                    TimeSpan.FromMinutes(2),
                    TimeSpan.FromMinutes(3), 
                }, (result, span) => {
		            if (result is null)
		    	        return;

                    var code = result.Result.StatusCode;
                    var logger = loggerGetter();
                            
                    logger.Warning($"Retrying after {span} (HTTP: {code})");
                    if (code == HttpStatusCode.ServiceUnavailable)
                        logger.Error("Cookies and/or CSRF token may be incorrect!");
                })
            );
        }

        private static Action<HttpClient> SetupClient(string baseUri) {
            return c => {
                c.BaseAddress = new Uri(baseUri);
                c.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", Constants.UserAgent);
            };
        }
    }
}
